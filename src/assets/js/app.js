var app = angular.module('app', ['ngRoute', 'ngAnimate', 'ui.bootstrap']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'producstListController',
            templateUrl: 'views/list.html'
        })
        .when('/product/details/:id', {
            controller: 'productDetailsController',
            templateUrl: 'views/details.html'
        })
        .when('/checkout', {
            controller: 'checkoutController',
            templateUrl: 'views/checkout.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});