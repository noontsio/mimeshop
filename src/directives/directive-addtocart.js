
app.directive('addToCartSection', ['cartServices', '$timeout', function (cartServices, $timeout) {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
            cart: "="
        },
        templateUrl: './views/partials/addtocart.html',

        link: function link(scope, element, attribs) {
            scope.quantity = 1;

            element.on('click', function (event) {

                // better way? 
                var Pid = scope.$parent.product.productId;

                cartServices.addItem(Pid, scope.quantity);

                //update cart icon quantity
                scope.cartlength = cartServices.cartlength();
                console.log(scope.cartlength)



                // $scope.badgeAnim = true;



                /// cart animation
                var cartElem = $(".cart-icon");
                cartElem.addClass('shakeit');
                $timeout(function () {
                    cartElem.removeClass('shakeit');
                    scope.badgeAnim = false;
                }, 700);


            });
        }
    };
}]);