
app.factory('productsFactory', ['$http', 'shoppingCartFactory', function ($http, shoppingCartFactory) {

    var products = {};

    products.getProducts = function () {
        return $http({
            method: 'GET',
            url: '/api/products'
        })

    };

    products.getProduct = function (id) {
        return $http({
            method: 'GET',
            url: '/api/products/' + id
        })

    };

    products.getShoppingCart = function () {
        return new Promise(function (resolve, reject) {
            var product, checkoutProducts = [];

            shoppingCartFactory.getCart().forEach(function (item) {
                $http({
                    method: 'GET',
                    url: '/api/products/' + item.productId
                }).then(function (response) {

                    product = response.data;
                    product.qta = item.quantity;
                    checkoutProducts.push(product);

                }, function (error) {
                    product.name = "not found";
                    product.qta = 0;
                    checkoutProducts.push(product);
                });

            })
            resolve(checkoutProducts);
        });
    }

    return products;


}]);