
app.factory('shoppingCartFactory', ['$window', function ($window) {


    var shoppingCart = {};
    var localStorageKey = "shopingcart";


    shoppingCart.getCart = function () {

        var cartContent = $window.localStorage[localStorageKey];

        if (cartContent) {
            cartContent = JSON.parse(cartContent)

        } else {
            cartContent = [];
        }

        return cartContent;
    };

    shoppingCart.saveCart = function (cart) {
        $window.localStorage[localStorageKey] = JSON.stringify(cart);
    };

    return shoppingCart;

}]);