app.service('cartServices', ['shoppingCartFactory', function (shoppingCartFactory) {


    this.cartlength = function () {

        return shoppingCartFactory.getCart().length;
    };

    this.cart = function () {

        return shoppingCartFactory.getCart();

    };

    this.updateQuantity = function (id, qta) {

        var quantity = parseInt(qta);
        var cart = shoppingCartFactory.getCart();

        cart.forEach(function (item) {

            if (item.productId == id) {
                item.quantity = quantity;
                found = true;
            }

        })

        shoppingCartFactory.saveCart(cart);
    };

    this.addItem = function (id, qta) {

        var quantity = parseInt(qta);
        var found = false;
        var cart = shoppingCartFactory.getCart();

        cart.forEach(function (item) {

            if (item.productId == id) {
                item.quantity += quantity;
                found = true;
            }

        })

        ///product not in cart ...add
        if (!found) {
            cart.push({
                productId: id,
                quantity: qta
            });
        }

        shoppingCartFactory.saveCart(cart);

    };


    this.removeItem = function (id) {

        var cart = shoppingCartFactory.getCart();

        cart.forEach(function (item, i) {

            if (item.productId == id) {
                cart.splice(i, 1);
            }

        });
        shoppingCartFactory.saveCart(cart);
    };


}]);