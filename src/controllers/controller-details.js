
app.controller('productDetailsController', ['$scope', '$http', '$routeParams', 'productsFactory', 'cartServices', '$timeout', function ($scope, $http, $routeParams, productsFactory, cartServices, $timeout) {

    $scope.pageHeading = "Product Detail";
    $scope.quantity = 1;
    $scope.badgeAnim = false;
    $scope.cartlength = cartServices.cartlength();

    $scope.getProduct = function () {

        var id = $routeParams.id;

        productsFactory.getProduct(id).then(function (response) {
            $scope.product = response.data

        }, function (error) {

        });
    }

    $scope.addToCart = function (id, qta) {
        cartServices.addItem(id, qta);
        $scope.cartlength = cartServices.cartlength();

        // cart animation TODO directive?
        $scope.badgeAnim = true;
        $timeout(function () { $scope.badgeAnim = false; }, 600);
    }

}]);