
app.controller('checkoutController', ['$scope', 'productsFactory', 'cartServices', '$timeout', function ($scope, productsFactory, cartServices, $timeout) {

    $scope.pageHeading = "Checkout";
    $scope.checkoutProducts = [];
    $scope.badgeAnim = false;
    $scope.cartlength = cartServices.cartlength();

    $scope.checkEmptyCart = function () {
        var empty = false;
        if ($scope.checkoutProducts.length == 0) {
            empty = true;
        }
        return empty;
    }

    $scope.getChekoutProducts = function () {
        productsFactory.getShoppingCart()
            .then(function (cart) {
                $scope.checkoutProducts = cart;
            }, function (error) {
                ////
            });

    }

    $scope.CalculateTotal = function () {
        var cartTotal = 0;
        $scope.checkoutProducts.forEach(function (item) {
            cartTotal += item.qta * item.price
        });
        return cartTotal;
    }

    $scope.productRemove = function (index, id) {
        cartServices.removeItem(id)

        //remove from view
        $scope.checkoutProducts.splice(index, 1);
        $scope.cartlength = cartServices.cartlength();
        $scope.cartAnimation();
    }

    $scope.updateCart = function (id, qta) {
        if (parseInt(qta) > 0) {
            cartServices.updateQuantity(id, qta)
            $scope.cartlength = cartServices.cartlength();
            // $scope.cartAnimation();
        }

    }

    $scope.cartAnimation = function () {

        $scope.badgeAnim = true;
        $timeout(function () { $scope.badgeAnim = false; }, 600);
    }

}]);