
app.controller('producstListController', ['$scope', '$http', 'productsFactory', 'cartServices', '$timeout', function ($scope, $http, productsFactory, cartServices, $timeout) {

    $scope.pageHeading = "Products List";
    // $scope.quantity = 1;
    // $scope.badgeAnim = false;

    $scope.cartlength = cartServices.cartlength() || 0;

    // $scope.cart = cartServices.cart() || {}


    $scope.getProductList = function () {
        productsFactory.getProducts().then(function (response) {
            $scope.products = response.data;
        }, function (error) {
            console.log(error);
        });

    }


    $scope.toggle = false;

    // $scope.addToCart = function (id, qta) {
    //     cartServices.addItem(id, qta);
    //     $scope.cartlength = cartServices.cartlength();

    //     // cart animation TODO directive?
    //     $scope.badgeAnim = true;
    //     $timeout(function () { $scope.badgeAnim = false; }, 600);

    // }

}]);