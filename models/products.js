// Hardecoded Products list
const AllProducts = require('../models/products-list.json');

// number of products in a collection
const AllProductsCount = Object.keys(AllProducts).length;


// ---------- the following functions simulates access to a database -----


/*
     retrive all products (missing pagination functionality)
*/
module.exports.getAllProducts = function (page, limit, callback) {

    // Pagination
    var pages = Math.ceil(AllProductsCount / limit)
    var offset = (page - 1) * limit;
    var err = false;

    if (offset > AllProductsCount) {
        err = true;
    }

    // simulate Select field1, field2 from table offset X, limit Y  
    var results = [];

    for (var x = 0; x < limit && x + offset < AllProductsCount; x++) {

        var productListIndex = x + offset;

        results.push({
            "productId": AllProducts[productListIndex].productId,
            "name": AllProducts[productListIndex].name,
            "shortDescription": AllProducts[productListIndex].shortDescription,
            "price": AllProducts[productListIndex].price,
            "image": AllProducts[productListIndex].images[0],
            "availableQuantity": AllProducts[productListIndex].availableQuantity,
        })
    }

    callback(err, results);

}

/*
    get a single product by ID
*/
module.exports.getProductById = function (id, callback) {

    // simulates select * from table where productId=X 
    var err = true;
    var selectedProduct = null;

    for (var i = 0; i < AllProductsCount; i++) {
        if (AllProducts[i].productId == id) {
            err = false;
            selectedProduct = AllProducts[i];
            break;
        }
    }

    callback(err, selectedProduct);

}

