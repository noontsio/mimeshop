## MimeShop test App ##

**MimeShop** 
e-commerce application test 

- server: Node and Express to simulate a RESTful API  
- front end: AngularJS, Bootstrap, AngularJS Bootstrap Carousel

## Usage TL;DR ##
- install dependencies with **npm install**  
- start the dev enviroment using  **node run dev** 


# Configuration #
**config.json**  allow to change all ports

# Development #
start the dev enviroment using  **node run dev** or **gulp dev**
a new Chrome window automatically will open at **http://localhost:7000/** (browserSync)
the node server will run on  **http://localhost:5001**

** what you get **

- browserSync : no need to browser refresh after code change
- nodemon : node server auto-restart after code change
- sass : to css
- inject : all plain css(after sass) and js files are injected in index.html 


# Production #
for local testing purpose
start the production enviroment using **node run prod** or **gulp build** 
access manually to **http://localhost:8080/** 
The production enviroment can also be run with **node run prod-debug** or **gulp prod --debug**  to help debugging the **map** files will be created for .js and .css files   

** what you get **

- uglify, concat, random-name  : all js and css files will be uglifyed, and concatenated in a random name (+timestamp) to avoid client browser cache
- imagemin : images size optimization
- htmlmin : html minification
- inject : all css and js files are injected in index.html




## Test Requests ##
Create an eCommerce MimeShop app that has at the least the following features:
-  List of products with price and an option to choose quantity (Image preview, short description, add to cart)
-  Product page (Images carousel, long description, add to cart)
-  Cart page / Checkout (the products which should show the total amount)

The app has to be written in AngularJS 1.x.x with Bootstrap 3.x.x and JS.
The backed to simulate the rest API calls has to be written in NodeJs and Express.
The code needs to have unit testing and easy for us to Run and written as it should go to production,

provided on github/bitbucket link ready to be cloned.




DEV WATCH
per i nuovi file.js o deteted js da injectare in index.html
