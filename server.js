const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const Products = require('./models/products');
const config = require('./config.json');

// Server App Config
var envFolder = "/dist/";
app.set('port', config.ports.nodeProd);

if (process.env.NODE_ENV == 'development') {
    app.set('port', config.ports.nodeDev);
    envFolder = "/src";
}
app.use(bodyParser.json());
app.use(express.static(__dirname + envFolder));


/// -------------------Routes --------------------- ///

// get products  /api/products/?page=1&limit=10
app.get('/api/products/', function (req, res) {

    var page = parseInt(req.query.page) || 1;
    var limit = parseInt(req.query.limit) || 15;

    Products.getAllProducts(page, limit, function (err, products) {

        if (err) {
            errorhandler(res, "no data to show")
        }

        res.json(products);
    });

});

// get a specific product by id   /api/products/X
app.get('/api/products/:id', function (req, res) {

    Products.getProductById(req.params.id, function (err, product) {

        if (err) {
            errorhandler(res, "record not found")
        }

        res.json(product);
    });
});

// error
function errorhandler(res, msg) {
    res.status(400).json({
        error: msg
    });
    throw msg;
}


app.listen(app.get('port'));
console.log('Node Server running on http://localhost:' + app.get('port'));
