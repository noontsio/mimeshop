const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const watch = require('gulp-watch');
const nano = require('gulp-cssnano');
const ngAnnotate = require('gulp-ng-annotate');
const inject = require('gulp-inject');
const del = require('del');
const changed = require('gulp-changed');
const randomize = require('randomatic');
const sass = require('gulp-sass');
const nodemon = require('gulp-nodemon');
const imagemin = require('gulp-imagemin');
const argv = require('yargs').argv;
const sourcemaps = require('gulp-sourcemaps');
const gulpif = require('gulp-if');


const filename = "mimeshop" + '_' + Date.now() + '_' + randomize('a0', 10);

var paths = {
    sass: {
        src: ['./src/assets/sass/**/*.scss']
    },
    css: {
        src: ['./src/assets/css/*.css']
    },
    js: {
        src: ['./src/assets/js/*.js', './src/controllers/*.js', './src/factory/*.js', './src/services/*.js', './src/directive/*.js']
    },
    views: {
        src: ['./src/views/**/*']
    },
    images: {
        src: ['./src/assets/images/**/*']
    },
    index: {
        src: ['./src/index.html']
    },

    dev: {
        css: {
            dest: 'src/assets/css/'
        },
        index: {
            fileToinject: ['./src/assets/css/*.css', './src/assets/js/*.js', './src/controllers/*.js', './src/factory/*.js', './src/services/*.js', './src/directive/*.js'],
            dest: './src/'
        }
    },
    prod: {
        views: {
            dest: './dist/views/'
        },
        js: {
            dest: './dist/assets/js/'
        },
        css: {
            dest: './dist/assets/css/'
        },
        images: {
            dest: './dist/assets/images/'
        },
        index: {
            fileToinject: ['./dist/assets/js/' + filename + '.js', './dist/assets/css/' + filename + '.css'],
            dest: './dist/'
        }
    }

}

//development
gulp.task('sass', function () {

    return gulp.src(paths.sass.src)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(paths.dev.css.dest))

});

gulp.task('index-dev', function () {
    var target = gulp.src(paths.index.src);

    return target.pipe(inject(
        gulp.src(paths.dev.index.fileToinject, { read: false }), { ignorePath: 'src', addRootSlash: false })
    ).pipe(gulp.dest(paths.dev.index.dest));

});

gulp.task('watch-dev', function () {
    gulp.watch(paths.sass.src, ['sass']);

});

gulp.task('start-server', ['watch-dev', 'index-dev'], function () {
    nodemon({
        script: 'server.js'
        , ext: 'js html'
        , env: { 'NODE_ENV': 'development' }
    })
})

///production

gulp.task('images', ['del-dist'], function () {

    return gulp.src(paths.images.src)
        .pipe(imagemin([
            imagemin.gifsicle({ interlaced: true }),
            imagemin.jpegtran({ progressive: true }),
            imagemin.optipng({ optimizationLevel: 5 }),
            imagemin.svgo({ plugins: [{ removeViewBox: true }] })
        ]))
        .pipe(gulp.dest(paths.prod.images.dest))
    // .pipe(browserSync.reload({ stream: true }));
});

gulp.task('css-prod', ['javascript-prod'], function () {

    return gulp.src(paths.css.src)
        .pipe(concat(filename + ".css"))
        .pipe(nano())
        .pipe(gulp.dest(paths.prod.css.dest));

});

gulp.task('javascript-prod', ['copy-views'], function () {

    return gulp.src(paths.js.src)
        .pipe(gulpif(argv.debug, sourcemaps.init()))
        .pipe(concat(filename + ".js"))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulpif(argv.debug, sourcemaps.write('')))
        .pipe(gulp.dest(paths.prod.js.dest));

});

gulp.task('copy-views', ['images'], function () {

    return gulp.src(paths.views.src)
        .pipe(gulp.dest(paths.prod.views.dest));

});

gulp.task('index-prod', ['css-prod'], function () {

    var target = gulp.src(paths.index.src);
    var sources = gulp.src(paths.prod.index.fileToinject, { read: false });

    return target.pipe(inject(sources, { ignorePath: 'dist', addRootSlash: false })
    ).pipe(gulp.dest(paths.prod.index.dest));

});

gulp.task('del-dist', function () {
    return del('./dist/**/*', { force: true });
});

gulp.task('build', ['index-prod']);
gulp.task('dev', ['index-dev', 'start-server']);
gulp.task('default');