const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const watch = require('gulp-watch');
const nano = require('gulp-cssnano');
const ngAnnotate = require('gulp-ng-annotate');
const inject = require('gulp-inject');
const del = require('del');
const changed = require('gulp-changed');
const randomize = require('randomatic');
const sass = require('gulp-sass');
const nodemon = require('gulp-nodemon');
const imagemin = require('gulp-imagemin');
const argv = require('yargs').argv;
const sourcemaps = require('gulp-sourcemaps');
const gulpif = require('gulp-if');
const runSequence = require('run-sequence');// gulp.series
const log = require('fancy-log');
const browserSync = require('browser-sync');
const config = require('./config.json');
var htmlmin = require('gulp-htmlmin');


const filename = "mimeshop" + '_' + Date.now() + '_' + randomize('a0', 10);

var paths = {
    sass: {
        src: ['./src/assets/sass/**/*.scss']
    },
    css: {
        src: ['./src/assets/css/*.css']
    },
    js: {
        src: ['./src/assets/js/*.js', './src/controllers/*.js', './src/factory/*.js', './src/services/*.js', './src/directives/*.js']
    },
    views: {
        src: ['./src/views/**/*']
    },
    images: {
        src: ['./src/assets/images/**/*']
    },
    index: {
        src: ['./src/index.html']
    },

    dev: {
        css: {
            dest: 'src/assets/css/'
        },
        index: {
            fileToinject: ['./src/assets/css/*.css', './src/assets/js/*.js', './src/controllers/*.js', './src/factory/*.js', './src/services/*.js', './src/directive/*.js'],
            dest: './src/'
        }
    },
    prod: {
        views: {
            dest: './dist/views/'
        },
        js: {
            dest: './dist/assets/js/'
        },
        css: {
            dest: './dist/assets/css/'
        },
        images: {
            dest: './dist/assets/images/'
        },
        index: {
            fileToinject: ['./dist/assets/js/' + filename + '.js', './dist/assets/css/' + filename + '.css'],
            dest: './dist/'
        }
    }

}

/*
    -----------------  development tasks -----------------------
*/

gulp.task('sass', function () {

    return gulp.src(paths.sass.src)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(paths.dev.css.dest))
        .pipe(browserSync.stream());
});

gulp.task('index-dev', function () {

    var target = gulp.src(paths.index.src);

    return target.pipe(inject(
        gulp.src(paths.js.src.concat(paths.css.src), { read: false }), { ignorePath: 'src', addRootSlash: false }))
        .pipe(gulp.dest(paths.dev.index.dest));


});

gulp.task('watch-dev', function () {
    gulp.watch(paths.sass.src, ['sass']);
    gulp.watch("src/*.html").on('change', browserSync.reload);
});

gulp.task('server-dev', ['nodemon'], function () {

    browserSync.init(null, {
        proxy: "http://localhost:" + config.ports.nodeDev,
        files: ["src/**/*.*"],
        browser: "google chrome",
        port: config.ports.clientDev,
    });


})

gulp.task('nodemon', function (cb) {

    var started = false;

    return nodemon({
        script: 'server.js'
        , ext: 'js'
        , env: { 'NODE_ENV': 'development' }
    }).on('start', function () {
        // to avoid nodemon being started multiple times
        // thanks @matthisk
        if (!started) {
            cb();
            started = true;
        }
    });
});

gulp.task('dev', function () {

    showMessage("Development Enviroment")

    runSequence(
        'index-dev',
        'watch-dev',
        'server-dev');
});


/*
  -----------------  production tasks ------------------
*/
gulp.task('server-production', function () {

    showMessage("any changes in DIST folder WILL BE LOST")

    nodemon({
        script: 'server.js'
        , ext: 'js html'
        , env: { 'NODE_ENV': 'production' }
    })
})

gulp.task('copy-images', function () {

    return gulp.src(paths.images.src)
        .pipe(imagemin([
            imagemin.gifsicle({ interlaced: true }),
            imagemin.jpegtran({ progressive: true }),
            imagemin.optipng({ optimizationLevel: 5 }),
            imagemin.svgo({ plugins: [{ removeViewBox: true }] })
        ]))
        .pipe(gulp.dest(paths.prod.images.dest))
});

gulp.task('copy-views', function () {
    return gulp.src(paths.views.src)
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(paths.prod.views.dest));

});

gulp.task('build-styles', function () {

    return gulp.src(paths.css.src)
        .pipe(gulpif(argv.debug, sourcemaps.init()))
        .pipe(concat(filename + ".css"))
        .pipe(nano())
        .pipe(gulpif(argv.debug, sourcemaps.write('')))
        .pipe(gulp.dest(paths.prod.css.dest));

});

gulp.task('build-scripts', function () {

    return gulp.src(paths.js.src)
        .pipe(gulpif(argv.debug, sourcemaps.init()))
        .pipe(concat(filename + ".js"))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulpif(argv.debug, sourcemaps.write('')))
        .pipe(gulp.dest(paths.prod.js.dest));

});

gulp.task('build-index', function () {

    var target = gulp.src(paths.index.src);
    var sources = gulp.src(paths.prod.index.fileToinject, { read: false });

    return target.pipe(inject(sources, { ignorePath: 'dist', addRootSlash: false }))
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(paths.prod.index.dest));

});

gulp.task('build-clean', function () {
    return del('./dist/**/*', { force: true });
});


// ---------------------------- main tasks -------------------------

gulp.task('build', function () {

    showMessage("Production enviroment")
    if (argv.debug) {
        showMessage("debug=true - map files created")
    } else {
        showMessage("debug=false - map files NOT created")

    }

    runSequence(
        'build-clean', //seq
        ['build-scripts', //async
            'build-styles',
            'copy-views',
            'copy-images'
        ],
        'build-index',
        'server-production');

});


gulp.task('default', ['dev']);


function showMessage(msg) {
    var line = "----------------------------------";
    log(line);
    log(msg);
    log(line);
}